// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseGeometryActor.h"
#include "HubActor.generated.h"

class ABaseGeometryActor;

USTRUCT(BlueprintType)
struct FGeometryInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		TSubclassOf<ABaseGeometryActor> GeometryClass;

	UPROPERTY(EditAnywhere)
		FGeometryData_1 myData;

	UPROPERTY(EditAnywhere)
		FTransform Transform;
};

UCLASS()
class SANDBOX_API AHubActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHubActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<ABaseGeometryActor> GeometryClass;

	void SpawnV1();
	void SpawnV2();
	void SpawnV3();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		TArray<FGeometryInfo> GeometryInfos;

	UFUNCTION()
	void OnColorChanged(const FLinearColor& Color, const FString& Name);
	void TimerFinished(AActor* Actor);
};
