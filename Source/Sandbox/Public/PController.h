// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PController.generated.h"

/**
 * 
 */
UCLASS()
class SANDBOX_API APController : public APlayerController
{
	GENERATED_BODY()
	
public:
	APController();

protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;


private:
	void ChangePawn();
	UPROPERTY()
	TArray<AActor*> Pawns;
	int32 CurrentPlayerIndex = 0;
};
