// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseGeometryActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeColor, const FLinearColor&, Color, const FString&, Name);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnTimerFinihsed, AActor*);

UENUM(BlueprintType)
enum class EMovementType : uint8
{
	Sin,
	Static
};

USTRUCT(BlueprintType)
struct FGeometryData_1
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float Amplitude = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float Frequency = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementType MovementType = EMovementType::Static;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FLinearColor myColor = FLinearColor::Green;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseVar")
		float TimerRate = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseVar")
	int32 MaxTimerCount = 10;
};

UCLASS()
class SANDBOX_API ABaseGeometryActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseGeometryActor();

	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* BaseMesh;

	UPROPERTY(BlueprintAssignable)
	FOnChangeColor OnChangeColor;
	FOnTimerFinihsed OnTimerFinihsed;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere, Category = "BaseVar")
	int32 Integer_01 = 4;
	UPROPERTY(EditDefaultsOnly, Category = "BaseVar")
	int32 Integer_02 = 10;
	UPROPERTY(EditInstanceOnly, Category = "BaseVar")
	float Float_01 = 12.134f;
	UPROPERTY(VisibleAnywhere, Category = "BaseVar")
	float Float_02 = 0.0f;
	UPROPERTY(EditAnywhere, Category = "BaseVar")
	bool bTest = false;
	UPROPERTY(EditAnywhere, Category = "BaseVar")
	bool bTest_01 = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Protected")
		FGeometryData_1 GeometryData;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


private:
	FVector InitialLocation;


	int32 TimerCount = 0;

	void DisplayLogs();

	void PrintStringTypes();

	void PrintTransform();

	void SetColor(FLinearColor &Color);

	void MoveSinTick();

public:
	void OnTimerFired();

	FTimerHandle TimerHandle;
	UFUNCTION(BlueprintCallable)
		FGeometryData_1 GetGeometryData() { return GeometryData; };
	UFUNCTION(BlueprintCallable)
		void SetGeometryData(FGeometryData_1 Data) { GeometryData = Data; };
};
