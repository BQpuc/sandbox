// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseGeometryActor.h"
#include "Engine/Engine.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "TimerManager.h"

DEFINE_LOG_CATEGORY_STATIC(MyLog, All, All)

// Sets default values
ABaseGeometryActor::ABaseGeometryActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>("BaseMesh");
	SetRootComponent(BaseMesh);

}

// Called when the game starts or when spawned
void ABaseGeometryActor::BeginPlay()
{
	Super::BeginPlay();
	
	InitialLocation = GetActorLocation();

	SetColor(GeometryData.myColor);
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ABaseGeometryActor::OnTimerFired, GeometryData.TimerRate, true);
}

void ABaseGeometryActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	UE_LOG(MyLog, Error, TEXT("%s DESTROYED"), *this->GetName());

	Super::EndPlay(EndPlayReason);
}

// Called every frame
void ABaseGeometryActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MoveSinTick();
}

void ABaseGeometryActor::DisplayLogs()
{
	UE_LOG(LogTemp, Display, TEXT("Hello Unreal"));
	UE_LOG(LogTemp, Warning, TEXT("Hello Unreal"));
	UE_LOG(LogTemp, Error, TEXT("Hello Unreal"));



	UE_LOG(LogTemp, Display, TEXT("Integer_01: %d, Integer_02: %i"), Integer_01, Integer_02);
	UE_LOG(LogTemp, Warning, TEXT("Float_01: %f, Float_02: %f"), Float_01, Float_02);
	UE_LOG(LogTemp, Error, TEXT("bTest: %d"), bTest);
	UE_LOG(LogTemp, Display, TEXT("bTest_01: %d"), static_cast<int>(bTest_01));
	UE_LOG(LogTemp, Display, TEXT("Float_01: %.1f"), Float_01);

	UE_LOG(MyLog, Display, TEXT("Test"));
}

void ABaseGeometryActor::PrintStringTypes()
{
	FString Name = "Natasha";
	UE_LOG(MyLog, Warning, TEXT("Name: %s"), *Name);

	FString TestVarInt = "Test int: " + FString::FromInt(Integer_01);
	FString TestVarFloat = "Test float: " + FString::SanitizeFloat(Float_01);
	FString TestVarBool = "Test bool: " + FString(bTest ? "true" : "false");

	FString Stat = FString::Printf(TEXT("All Variables \n %s %s %s"), *TestVarInt, *TestVarFloat, *TestVarBool);
	UE_LOG(MyLog, Warning, TEXT("%s"), *Stat);

	UE_LOG(MyLog, Warning, TEXT("Actor Name %s"), *GetName());
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Orange, Stat);
		GEngine->AddOnScreenDebugMessage(-1, 8.0f, FColor::Red, TestVarBool, true, FVector2D(1.5f, 1.5f));
	}
}

void ABaseGeometryActor::PrintTransform()
{
	FTransform Transform = GetActorTransform();
	FVector Location = Transform.GetLocation();
	FRotator Rotation = Transform.Rotator();
	FVector Scale = Transform.GetScale3D();

	UE_LOG(MyLog, Warning, TEXT("Transform %s"), *Transform.ToString());
	UE_LOG(MyLog, Warning, TEXT("Location %s"), *Location.ToString());
	UE_LOG(MyLog, Warning, TEXT("Rotation %s"), *Rotation.ToString());
	UE_LOG(MyLog, Warning, TEXT("Scale %s"), *Scale.ToString());

	//UE_LOG(My_Log, Error, TEXT("Human transform %s"), *Transform.ToHumanReadableString());
}

void ABaseGeometryActor::SetColor(FLinearColor &Color)
{
	if (!BaseMesh)
		return;
	UMaterialInstanceDynamic* MatInstance = BaseMesh->CreateAndSetMaterialInstanceDynamic(0);
	if (MatInstance)
	{
		MatInstance->SetVectorParameterValue("Color", Color);
	}
}

void ABaseGeometryActor::MoveSinTick()
{
	switch (GeometryData.MovementType)
	{
	case EMovementType::Sin:
		{
		FVector CurrentLocation = GetActorLocation();
		if (GetWorld())
		{
			float time = GetWorld()->GetTimeSeconds();
			CurrentLocation.Z = InitialLocation.Z + GeometryData.Amplitude * FMath::Sin(GeometryData.Frequency * time);
			SetActorLocation(CurrentLocation);
		}
		break;
		}
	case EMovementType::Static:
		break;
	default:
		break;
	}
}

void ABaseGeometryActor::OnTimerFired()
{
	if (++TimerCount <= GeometryData.MaxTimerCount)
	{
		FLinearColor NewColor = FLinearColor::MakeRandomColor();
		UE_LOG(MyLog, Warning, TEXT("Count: %i Color: %s"), TimerCount, *NewColor.ToString());
		SetColor(NewColor);
		OnChangeColor.Broadcast(NewColor, GetName());
	}
	else
	{
		UE_LOG(MyLog, Error, TEXT("Timer Material Stop"));
		GetWorldTimerManager().ClearTimer(TimerHandle);
		OnTimerFinihsed.Broadcast(this);
	}
}





