// Fill out your copyright notice in the Description page of Project Settings.

#include "PController.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "SandboxPawn.h"

DEFINE_LOG_CATEGORY_STATIC(ControllerLog, All, All)

APController::APController()
{

}

void APController::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASandboxPawn::StaticClass(), Pawns);
}

void APController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (InputComponent)
	{
		InputComponent->BindAction("ChangePawn", IE_Pressed, this, &APController::ChangePawn);
	}
}

void APController::ChangePawn()
{
	if (Pawns.Num() <= 1) return;

	ASandboxPawn* CurrentPawn = Cast<ASandboxPawn>(Pawns[CurrentPlayerIndex]);
	CurrentPlayerIndex = (CurrentPlayerIndex + 1) % Pawns.Num();
	if (!CurrentPawn) return;

	UE_LOG(ControllerLog, Error, TEXT("Pawn Chahged"));
	Possess(CurrentPawn);


}