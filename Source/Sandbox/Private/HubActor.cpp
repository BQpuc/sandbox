// Fill out your copyright notice in the Description page of Project Settings.

#include "HubActor.h"
#include "Engine/World.h"

DEFINE_LOG_CATEGORY_STATIC(HubLog, All, All)

// Sets default values
AHubActor::AHubActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHubActor::BeginPlay()
{
	Super::BeginPlay();
	
	//SpawnV1();
	//SpawnV2();
	SpawnV3();
}

void AHubActor::SpawnV1()
{
	if (GetWorld())
	{
		for (int32 i = 0; i < 10; ++i)
		{
			const FTransform GeometryTransform = FTransform(FRotator::ZeroRotator, FVector(0.0f, 300.0f * i, 0.0f));
			ABaseGeometryActor* Geometry = GetWorld()->SpawnActor<ABaseGeometryActor>(GeometryClass, GeometryTransform);
			if (Geometry)
			{
				FGeometryData_1 Data = Geometry->GetGeometryData();
				Data.MovementType = FMath::RandBool() ? EMovementType::Sin : EMovementType::Static;
				Geometry->SetGeometryData(Data);
			}
		}
	}
}

void AHubActor::SpawnV2()
{
	if (GetWorld())
	{
		for (int32 i = 0; i < 10; ++i)
		{
			const FTransform GeometryTransform = FTransform(FRotator::ZeroRotator, FVector(0.0f, 300.0f * i, 500.0f));
			ABaseGeometryActor* Geometry = GetWorld()->SpawnActorDeferred<ABaseGeometryActor>(GeometryClass, GeometryTransform);
			if (Geometry)
			{
				FGeometryData_1 Data = Geometry->GetGeometryData();
				Data.myColor = FLinearColor::MakeRandomColor();
				Data.MovementType = FMath::RandBool() ? EMovementType::Sin : EMovementType::Static;
				Geometry->SetGeometryData(Data);
				Geometry->FinishSpawning(GeometryTransform);
			}
		}
	}
}

void AHubActor::SpawnV3()
{
	if (GetWorld())
	{
		for (const FGeometryInfo Info : GeometryInfos)
		{
			ABaseGeometryActor* Geometry = GetWorld()->SpawnActorDeferred<ABaseGeometryActor>(Info.GeometryClass, Info.Transform);
			if (Geometry)
			{
				Geometry->SetGeometryData(Info.myData);
				Geometry->OnChangeColor.AddDynamic(this, &AHubActor::OnColorChanged);
				Geometry->OnTimerFinihsed.AddUObject(this, &AHubActor::TimerFinished);
				Geometry->FinishSpawning(Info.Transform);
			}
		}
	}
}

// Called every frame
void AHubActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHubActor::OnColorChanged(const FLinearColor& Color, const FString& Name)
{
	UE_LOG(HubLog, Warning, TEXT("Name: %s, Color: %s"), *Color.ToString(), *Name);

}

void AHubActor::TimerFinished(AActor* Actor)
{
	if (!Actor) return;
	UE_LOG(HubLog, Error, TEXT(" %s \n Timer Finish"), *Actor->GetName());
	ABaseGeometryActor* GeometryActor = Cast<ABaseGeometryActor>(Actor);
	if (GeometryActor)
	{
		GeometryActor->Destroy();
	}
}

